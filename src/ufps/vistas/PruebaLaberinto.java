/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ejercicio.Laberinto;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author javier
 */
public class PruebaLaberinto {

    public static void main(String[] args) {
        try {
            Laberinto l = new Laberinto("https://gitlab.com/madarme/archivos-persistencia/raw/master/laberinto/matrizLaberintoTeseo.txt");
            String coordenadas = l.getCamino();
            System.out.println(coordenadas);
            String mapa;
            mapa = l.getMapa(coordenadas);
            System.out.println(mapa);
            Laberinto l2 = new Laberinto("http://madarme.co/persistencia/laberintoTeseo2.txt");
            String coordenadas2 = l2.getCamino();
            System.out.println(coordenadas2);
            String mapa2  = l2.getMapa(coordenadas2);
            System.out.println(mapa2);
            Laberinto l3 = new Laberinto("http://madarme.co/persistencia/laberintoTeseo3.txt");
            String coordenadas3 = l3.getCamino();
            System.out.println(coordenadas3);
        } catch (Exception ex) {
            ex.printStackTrace();;
        }
    }
}
