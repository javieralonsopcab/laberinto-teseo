/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio;

import ufps.util.colecciones_seed.ColaP;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author javier
 */
public class Laberinto {

    private short laberinto[][];
    ColaP<String> posiblesCaminos = new ColaP<>();

    public Laberinto(String url) {
        ArchivoLeerURL leer = new ArchivoLeerURL(url);
        Object datos[] = leer.leerArchivo();
        this.laberinto = new short[datos.length][];
        for (int i = 0; i < datos.length; i++) {
            String[] fila = datos[i].toString().split(";");
            this.laberinto[i] = new short[fila.length];
            for (int j = 0; j < laberinto[i].length; j++) {
                this.laberinto[i][j] = Short.parseShort(fila[j]);
            }
        }
    }

    public String getCamino() {
        try {
            String[] coordenadasTeseo = this.buscarCoordenadasDe(9, this.laberinto).split(",");
            short filaTeseo = Short.parseShort(coordenadasTeseo[0]);
            short columnaTeseo = Short.parseShort(coordenadasTeseo[1]);
            String[] coordenadasMinotauro = this.buscarCoordenadasDe(100, this.laberinto).split(",");
            short filaMinotauro = Short.parseShort(coordenadasMinotauro[0]);
            short columnaMinotauro = Short.parseShort(coordenadasMinotauro[1]);
            String[] coordenadasSalida = this.buscarCoordenadasDe(1000, this.laberinto).split(",");
            short filaSalida = Short.parseShort(coordenadasSalida[0]);
            short columnaSalida = Short.parseShort(coordenadasSalida[1]);
            this.getCamino(filaSalida, columnaSalida, filaTeseo, columnaTeseo, filaMinotauro, columnaMinotauro,
                    this.laberinto, "", false, false);
            if (!this.posiblesCaminos.esVacia()) {
                return this.posiblesCaminos.deColar();
            } else {
                return "No tiene solución";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return "Hubo un error en la matriz";
        }
    }

    private void getCamino(short filaSalida, short columnaSalida, int filaTeseo, int colTeseo, int filaMinotauro,
            int colMintauro, short[][] laberinto, String msg,
            boolean matoMinotauro, boolean encontroSalida) {
        msg += "(" + filaTeseo + "," + colTeseo + ")";
        short anterior = 0;
        if (laberinto[filaTeseo][colTeseo] == 0) {
            laberinto[filaTeseo][colTeseo] = 1;
            anterior = 0;
        }
        if (filaTeseo == filaMinotauro && colTeseo == colMintauro) {
            laberinto[filaTeseo][colTeseo] = -100;
            matoMinotauro = true;
            anterior = 100;
        }
        if (filaTeseo == filaSalida && columnaSalida == colTeseo && matoMinotauro) {
            encontroSalida = true;
        }
        int arriba = this.numeroArriba(filaTeseo, colTeseo, laberinto);
        int abajo = this.numeroAbajo(filaTeseo, colTeseo, laberinto);
        int derecha = this.numeroAladerecha(filaTeseo, colTeseo, laberinto);
        int izquierda = this.numeroAlaIzquierda(filaTeseo, colTeseo, laberinto);
        if (arriba != -1) {
            if (laberinto[filaTeseo - 1][colTeseo] != 9) {
                this.getCamino(filaSalida, columnaSalida, filaTeseo - 1, colTeseo, filaMinotauro, colMintauro, laberinto,
                        msg, matoMinotauro, encontroSalida);
                laberinto[filaTeseo - 1][colTeseo] = anterior;
            }
        }
        if (izquierda != -1) {
            if (laberinto[filaTeseo][colTeseo - 1] != 9) {
                this.getCamino(filaSalida, columnaSalida, filaTeseo, colTeseo - 1, filaMinotauro, colMintauro, laberinto,
                        msg, matoMinotauro, encontroSalida);
                laberinto[filaTeseo][colTeseo - 1] = anterior;
            }
        }
        if (abajo != -1) {
            if (laberinto[filaTeseo + 1][colTeseo] != 9) {
                this.getCamino(filaSalida, columnaSalida, filaTeseo + 1, colTeseo, filaMinotauro, colMintauro, laberinto,
                        msg, matoMinotauro, encontroSalida);
                laberinto[filaTeseo + 1][colTeseo] = anterior;
            }
        }
        if (derecha != -1) {
            if (laberinto[filaTeseo][colTeseo + 1] != 9) {
                this.getCamino(filaSalida, columnaSalida, filaTeseo, colTeseo + 1, filaMinotauro, colMintauro, laberinto,
                        msg, matoMinotauro, encontroSalida);
                laberinto[filaTeseo][colTeseo + 1] = anterior;
            }
        }
        if (encontroSalida && matoMinotauro) {
            this.posiblesCaminos.enColar(msg, -(msg.length()));
        }
    }

    private boolean puedeAvanzarArriba(int filaTeseo, int colTeseo, short matriz[][]) {
        if (filaTeseo < matriz.length && filaTeseo > 0) {
            return matriz[filaTeseo - 1][colTeseo] != -1 && matriz[filaTeseo - 1][colTeseo] != 1
                    && matriz[filaTeseo - 1][colTeseo] != -100;
        }
        return false;
    }

    private int numeroArriba(int filaTeseo, int colTeseo, short matriz[][]) {
        if (this.puedeAvanzarArriba(filaTeseo, colTeseo, matriz)) {
            return matriz[filaTeseo - 1][colTeseo];
        }
        return -1;
    }

    private boolean puedeAvanzarDerecha(int filaTeseo, int colTeseo, short matriz[][]) {
        if (matriz[0].length - 1 > colTeseo) {
            return matriz[filaTeseo][colTeseo + 1] != -1 && matriz[filaTeseo][colTeseo + 1] != 1
                    && matriz[filaTeseo][colTeseo + 1] != -100;
        }
        return false;
    }

    private int numeroAladerecha(int filaTeseo, int colTeseo, short matriz[][]) {
        if (this.puedeAvanzarDerecha(filaTeseo, colTeseo, matriz)) {
            return matriz[filaTeseo][colTeseo + 1];
        }
        return -1;
    }

    private boolean puedeAvanzarAbajo(int filaTeseo, int colTeseo, short matriz[][]) {
        if (matriz.length - 1 > filaTeseo) {
            return matriz[filaTeseo + 1][colTeseo] != -1 && matriz[filaTeseo + 1][colTeseo] != 1
                    && matriz[filaTeseo + 1][colTeseo] != -100;
        }
        return false;
    }

    private int numeroAbajo(int filaTeseo, int colTeseo, short matriz[][]) {
        if (this.puedeAvanzarAbajo(filaTeseo, colTeseo, matriz)) {
            return matriz[filaTeseo + 1][colTeseo];
        }
        return -1;
    }

    private boolean puedeAvanzarIzquierda(int filaTeseo, int colTeseo, short matriz[][]) {
        if (matriz[0].length > colTeseo && colTeseo > 0) {
            return matriz[filaTeseo][colTeseo - 1] != -1 && matriz[filaTeseo][colTeseo - 1] != 1
                    && matriz[filaTeseo][colTeseo - 1] != -100;
        }
        return false;
    }

    private int numeroAlaIzquierda(int filaTeseo, int colTeseo, short matriz[][]) {
        if (this.puedeAvanzarIzquierda(filaTeseo, colTeseo, matriz)) {
            return matriz[filaTeseo][colTeseo - 1];
        }
        return -1;
    }

    private String buscarCoordenadasDe(int objeto, short[][] laberinto) {
        for (int i = 0; i < laberinto.length; i++) {
            for (int j = 0; j < laberinto[i].length; j++) {
                if (laberinto[i][j] == objeto) {
                    return i + "," + j;
                }
            }
        }
        return null;
    }

    public String toString() {
        String msg = "";
        for (int i = 0; i < this.laberinto.length; i++) {
            for (int j = 0; j < this.laberinto[i].length; j++) {
                msg += this.laberinto[i][j] + "         ";
            }
            msg += "\n";
        }
        return msg;
    }

    //(8,1)(7,1)(6,1)(6,0)(5,0)(4,0)(4,1)(4,2)(4,3)(5,3)(6,3)(7,3)(8,3)(8,4)
    public String getMapa(String coordenadas) throws Exception {
        if (coordenadas == null) {
            throw new Exception("No hay coordenadas");
        }
        for (int i = 0; i < coordenadas.length(); i++) {
            int x = (int) (coordenadas.charAt(++i) - 48);
            i++;
            int y = (int) (coordenadas.charAt(++i) - 48);
            i++;
            this.laberinto[x][y] = 1;
        }
        return this.toString();
    }
}
